# README #

This is a sample web app for the AWS mixer showing how to use the [S3 proxy](http://www.aerobatic.com/docs/) for your Aerobatic hosted web app.

The web app can be viewed at [http://aws-mixer.aerobatic.io/](http://aws-mixer.aerobatic.io/)